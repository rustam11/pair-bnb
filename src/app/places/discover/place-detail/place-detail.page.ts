import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {
  NavController,
  ModalController,
  ActionSheetController,
  LoadingController, AlertController
} from '@ionic/angular';
import {Place} from '../../place.model';
import {PlacesService} from '../../places.service';
import {CreateBookingComponent} from 'src/app/bookings/create-booking/create-booking.component';
import {Subscription} from 'rxjs';
import {BookingService} from 'src/app/bookings/booking.service';
import {AuthService} from 'src/app/auth/auth.service';
import {MapModalComponent} from '../../../shared/map-modal/map-modal.component';
import {switchMap, take} from 'rxjs/operators';

@Component({
  selector: 'app-place-detail',
  templateUrl: './place-detail.page.html',
  styleUrls: ['./place-detail.page.scss']
})
export class PlaceDetailPage implements OnInit, OnDestroy {
  place: Place;
  isBookable = false;
  isLoading = false;
  private placesSub: Subscription;

  constructor(
      private router: Router,
      private navCtrl: NavController,
      private route: ActivatedRoute,
      private placesService: PlacesService,
      private modalCtrl: ModalController,
      private actionSheetController: ActionSheetController,
      private bookingService: BookingService,
      private loadingCrtl: LoadingController,
      private authService: AuthService,
      private alertCtrl: AlertController
  ) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      if (!paramMap.has('placeId')) {
        this.navCtrl.navigateBack('/places/tabs/discover');
        return;
      }
      this.isLoading = true;
      let fetchedUserId: string;
      return this.authService.userId.pipe(take(1), switchMap(userId => {
            if (!userId) {
              throw new Error('No user id found');
            }
            fetchedUserId = userId;
            return this.placesService.getPlace(paramMap.get('placeId'));
          })
      ).subscribe(place => {
        this.place = place;
        this.isBookable = place.userId !== fetchedUserId;
        this.isLoading = false;
      }, error => {
        this.alertCtrl.create({
          header: 'An error occurred',
          message: 'Could not load place',
          buttons: [{
            text: 'Okay',
            handler: () => {
              this.router.navigate(['/places/tabs/discover']);
            }
          }]
        }).then(alertEl => {
          alertEl.present();
        });
      });
    });
  }


  onBookPlace() {
    this.actionSheetController
        .create({
          header: 'Choose an Action',
          buttons: [
            {
              text: 'Select Date',
              handler: () => {
                this.openBookingModal('select');
              }
            },
            {
              text: 'Random Dates',
              handler: () => {
                this.openBookingModal('random');
              }
            },
            {text: 'Cancel', role: 'destructive'}
          ]
        })
        .then(actionSheetEl => {
          actionSheetEl.present();
        });
  }

  openBookingModal(mode: 'select' | 'random') {
    this.modalCtrl
        .create({
          component: CreateBookingComponent,
          componentProps: {selectedPlace: this.place, selectedMode: mode}
        })
        .then(modalEl => {
          modalEl.present();
          return modalEl.onDidDismiss();
        })
        .then(resultData => {
          if (resultData.role === 'confirm') {
            this.loadingCrtl
                .create({
                  message: 'Booking place...'
                })
                .then(loadingEl => {
                  loadingEl.present();
                  const data = resultData.data.bookingData;
                  this.bookingService
                      .addBooking(
                          this.place.id,
                          this.place.title,
                          this.place.imageUrl,
                          data.firstName,
                          data.lastName,
                          data.guestNumber,
                          data.startDate,
                          data.endDate
                      )
                      .subscribe(res => {
                        loadingEl.dismiss();
                      });
                });
          }
        });
  }

  onShowFullMap() {
    this.modalCtrl.create({
      component: MapModalComponent,
      componentProps: {
        center: {lat: this.place.location.lat, lng: this.place.location.lng},
        selectable: false,
        closeButtonText: 'Close',
        title: this.place.location.address
      }
    }).then(modalEl => {
      modalEl.present();
    });
  }


  ngOnDestroy(): void {
    if (this.placesSub) {
      this.placesSub.unsubscribe();
    }
  }
}
