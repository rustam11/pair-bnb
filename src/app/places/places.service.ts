import {Injectable} from '@angular/core';
import {Place} from './place.model';
import {AuthService} from '../auth/auth.service';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {map, take, tap, switchMap} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {PlaceLocation} from './location.model';

interface PlaceData {
  availableFrom: string;
  availableTo: string;
  description: string;
  imageUrl: string;
  price: number;
  title: string;
  userId: string;
  location: PlaceLocation;
}

@Injectable({
  providedIn: 'root'
})
export class PlacesService {
  private _places = new BehaviorSubject<Place[]>([]);

  constructor(private authService: AuthService, private http: HttpClient) {
  }

  get places() {
    return this._places.asObservable();
  }

  fetchPlaces() {
    return this.http.get <{ [key: string]: PlaceData }>
    ('https://ionic-pair-bnb.firebaseio.com/offered-places.json')
        .pipe(map(resData => {
              const places = [];
              for (const key in resData) {
                if (resData.hasOwnProperty(key)) {
                  places.push(new Place(
                      key,
                      resData[key].title,
                      resData[key].description,
                      resData[key].imageUrl,
                      resData[key].price,
                      new Date(resData[key].availableTo),
                      new Date(resData[key].availableFrom),
                      resData[key].userId,
                      resData[key].location
                      )
                  );
                }
              }
              return places;
            }),
            tap(places => {
              this._places.next(places);
            })
        );
  }

  getPlace(id: string): Observable<Place> {
    return this.http.get<PlaceData>(`https://ionic-pair-bnb.firebaseio.com/offered-places/${id}.json`
    ).pipe(map(placeData => {
      return new Place(
          id,
          placeData.title,
          placeData.description,
          placeData.imageUrl,
          placeData.price,
          new Date(placeData.availableFrom),
          new Date(placeData.availableTo),
          placeData.userId,
          placeData.location
      );
    }));
  }

  addPlace(
      title: string,
      decription: string,
      price: number,
      dateFrom: Date,
      dateTo: Date,
      location: PlaceLocation
  ) {
    let generatedId: string;
    let newPlace: Place;
    return this.authService.userId.pipe(take(1), switchMap(userId => {
          if (!userId) {
            throw new Error('No user id found!');
          }
          newPlace = new Place(
              Math.random().toString(),
              title,
              decription,
              'https://media.nature.com/w800/magazine-assets/d41586-019-00862-y/d41586-019',
              price,
              dateFrom,
              dateTo,
              userId,
              location
          );
          return this.http
              .post<{ name: string }>(
                  'https://ionic-pair-bnb.firebaseio.com/offered-places.json',
                  {...newPlace, id: null});
        }), switchMap(resData => {
          generatedId = resData.name;
          return this.places;
        }),
        take(1),
        tap(places => {
          newPlace.id = generatedId;
          this._places.next(places.concat(newPlace));
        })
    );
  }

  updateOffer(placeId: string, title: string, description: string) {
    let updatedPlaces: Place[];
    return this.places.pipe(
        take(1),
        switchMap(places => {
          if (!places || places.length <= 0) {
            this.fetchPlaces();
          } else {
            return of(places);
          }
        }),

        switchMap(places => {
          const updatedPlaceIndex = places.findIndex(pl => pl.id === placeId);
          updatedPlaces = [...places];
          const oldPlace = updatedPlaces[updatedPlaceIndex];
          updatedPlaces[updatedPlaceIndex] = new Place(
              oldPlace.id,
              title,
              description,
              oldPlace.imageUrl,
              oldPlace.price,
              oldPlace.availableFrom,
              oldPlace.availableTo,
              oldPlace.userId,
              oldPlace.location
          );
          return this.http.put(
              `https://ionic-pair-bnb.firebaseio.com/offered-places/${placeId}.json`,
              {...updatedPlaces[updatedPlaceIndex], id: null}
          );
        }),
        tap(() => {
          this._places.next(updatedPlaces);
        })
    );
  }
}
